function [x, y, p0] = solve_ap3 (sigma)
    f = @(x, a, b) a - 2.*log(cosh(b.*(x - 1/2)));
    n = length(sigma);
    num_sol = 2;
    initial_guess = [0.5 10.8];
    p0 = zeros(n,num_sol);
    a = zeros(n,num_sol);
    b = zeros(n,num_sol);
    y = zeros(n,num_sol);
    x = 0.5;

    p0(1,:) = shoot_method(sigma(1), initial_guess);
    for j=1:num_sol
        try
            b(1,j) = fzero(@(b) 2*b*(tanh(b-b/2))-p0(1,j), 1);
            a(1,j) = fzero(@(a) a - 2*log(cosh(b(1,j)*(1/2))), 0.5);
        catch
            b(1,j) = NaN;
            a(1,j) = NaN;
        end
    end

    for i=2:n
        p0(i,:) = shoot_method(sigma(i), initial_guess);
        for j=1:num_sol
            try
                b(i,j) = fzero(@(b) 2*b*(tanh(b-b/2))-p0(i,j), b(i-1, j));
                a(i,j) = fzero(@(a) a - 2*log(cosh(b(i,j)*(1/2))), a(i-1,j));
            catch
                b(1,j) = NaN;
                a(1,j) = NaN;
            end
        end
    end
    for i=1:n
        try
            y(i,:) = f(x, a(i,:), b(i,:));
        catch
            y(i,:) = NaN;
        end
    end
    plot(sigma,y);

end
